package application.client.controllers;

import application.client.init.App;
import application.client.views.MainFrame;

public class BombermanClient{

	// Constants
	public static final String TITLE = "Bomberman";
	public static final int SIZE = App.DIMENSION / 20;
	private Controller lastCtrl;
	private MainFrame mainFrame;
	
	private static BombermanClient bombermanClient;
	
	public static BombermanClient getInstance(){
		if(bombermanClient == null){
			bombermanClient = new BombermanClient();
		}
		return bombermanClient;
	}
	
	private BombermanClient(){
		
		this.mainFrame = new MainFrame(10, 10, App.DIMENSION, App.DIMENSION + 75);
		
	}
	
	public void startFlow(){
		MenuController menuCtrl = MenuController.getInstance();
		this.display(menuCtrl);
	}
	
	// Display
	public void display(Controller ctrl){
		if(null != this.lastCtrl){
			this.lastCtrl.setVisible(false);
		}
		this.mainFrame.add(ctrl.getPanel());
		this.lastCtrl = ctrl;
		this.lastCtrl.setVisible(true);
	}
	
}

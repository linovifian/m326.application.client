package application.client.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;


import application.client.models.Connector;
import application.client.views.MenuPanel;
import application.client.views.Tile;

public class MenuController extends Controller{

	private MenuPanel menuPanel;
	private static MenuController menuController;
	
	private MenuController(){
		this.menuPanel = new MenuPanel();
		
		this.menuPanel.getBtnExit().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				btnExit_Click(e);
			}
		});
		
		this.menuPanel.getBtnFindGame().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				btnFindGame_Click(e);
			}			
		});
	}
	
	private void btnFindGame_Click(ActionEvent e){
		try {
			Connector.joinGame(this.menuPanel.getPlayerName());
		} catch(Exception ex){
			this.menuPanel.setErrorLabelVisible();
		}
	}
	
	private void btnExit_Click(ActionEvent e){
		System.exit(0);
	}
	
	public void displayGamePanel(Tile[][] tiles){
		GameController gameCtrl = GameController.getInstance();
		gameCtrl.setFields(tiles);
		BombermanClient.getInstance().display(gameCtrl);
	}
	
	
	@Override
	public void setVisible(boolean visible) {
		this.menuPanel.setVisible(visible);
	}

	@Override
	public JPanel getPanel() {
		return this.menuPanel;
		
	}

	public static MenuController getInstance() {
		if(menuController == null){
			menuController = new MenuController();
		}
		return menuController;
	}

}

package application.client.controllers;

import javax.swing.JPanel;

public abstract class Controller {
	
	public abstract void setVisible(boolean visible);
	
	public abstract JPanel getPanel();
}

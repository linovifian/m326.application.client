package application.client.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import application.client.views.Air;
import application.client.views.Block;
import application.client.views.Bomb;
import application.client.views.GamePanel;
import application.client.views.Player;
import application.client.views.Tile;

public class GameController extends Controller {

	private static final int IFW = JComponent.WHEN_IN_FOCUSED_WINDOW;
	private GamePanel gamePanel;
	private Tile[][] fields;
	private Player[] players;
	private Player mainPlayer;
	private static GameController gameController;
	
	public static GameController getInstance(){
		if(gameController == null){
			gameController = new GameController();
		}
		return gameController;
	}
	
	private GameController(){
		this.gamePanel = new GamePanel(); 
		this.fields = new Tile[20][20];
		this.mainPlayer = new Player("Faggot",1);
		players = new Player[1];
		players[0] = this.mainPlayer;
		
		this.gamePanel.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_W, 0),PlayerActions.MOVE_UP);
		this.gamePanel.getActionMap().put(PlayerActions.MOVE_UP,new UpAction(this.mainPlayer));
		this.gamePanel.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, 0),PlayerActions.MOVE_DOWN);
		this.gamePanel.getActionMap().put(PlayerActions.MOVE_DOWN,new DownAction(this.mainPlayer));
		this.gamePanel.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_A, 0),PlayerActions.MOVE_LEFT);
		this.gamePanel.getActionMap().put(PlayerActions.MOVE_LEFT,new LeftAction(this.mainPlayer));
		this.gamePanel.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_D, 0),PlayerActions.MOVE_RIGHT);
		this.gamePanel.getActionMap().put(PlayerActions.MOVE_RIGHT,new RightAction(this.mainPlayer));
		this.gamePanel.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0),PlayerActions.PLACE_BOMB);
		this.gamePanel.getActionMap().put(PlayerActions.PLACE_BOMB,new SpaceAction(this.mainPlayer));
		
		drawField();
		drawPlayers();
		
	}
	
	@Override
	public void setVisible(boolean visible) {
		this.gamePanel.setVisible(visible);
		
	}

	@Override
	public JPanel getPanel() {
		return this.gamePanel;
	}
	
	public void setFields(Tile[][] fields){
		this.fields = fields;
//		for (int i = 0; i < this.fields.length; i++){
//			for (int j = 0; j < this.fields[0].length; j++){
//				int rand = (int)(Math.random() * 4);
//				Tile newTile;
//				switch (rand){
//				case 0:
//					newTile = new Block(i,j,true);
//					break;
//				case 1:
//					newTile = new Block(i,j,false);
//					break;
//				case 2:
//					newTile = new Bomb(i,j);
//					break;
//				default:
//					newTile = new Air(i,j);
//					break;				
//				}
//				this.fields[i][j] = newTile;
//			}
//		}
	}
	
	private void drawField(){
		for (int i = 0; i < fields.length; i++){
			for (int j = 0; j < fields[0].length; j++){
				this.gamePanel.add(fields[i][j].drawTile());
			}
		}
		this.gamePanel.repaint();
	}
	
	private void drawPlayers(){		
		for (Player p : players){
			this.gamePanel.add(p.drawPlayer());
		}
		this.gamePanel.repaint();
	}
	
	private void refreshFields(){
		for (int i = 0; i < fields.length; i++){
			for (int j = 0; j < fields[0].length; j++){
				fields[i][j].refreshTile();
			}
		}
		this.gamePanel.repaint();
	}
	
	private void refreshPlayers(){		
		for (Player p : players){
			p.refreshPlayer();
		}
		this.gamePanel.repaint();
	}
	
	public void bombSet(int x, int y){
		fields[x][y] = new Bomb(x, y);
		refreshFields();
	}
	
	public void bombExploded(int x, int y){
		// Let the Bomb explode
		fields[x][y] = new Air(x, y);
		refreshFields();
	}
	
	private enum PlayerActions {
		MOVE_UP("up"),
		MOVE_DOWN("down"),
		MOVE_LEFT("left"),
		MOVE_RIGHT("right"),
		PLACE_BOMB("place"),
		;
		private final String action;
		
		PlayerActions(String action){
			this.action = action;
		}
	}
	
	private class UpAction extends AbstractAction {
		private Player p;
		public UpAction(Player player){
			this.p = player;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("w pressed");
			p.setPlayerY(p.getPlayerY() - 10);
			p.refreshPlayer();
			//Communication player moved
		}
	}
	
	private class DownAction extends AbstractAction {
		private Player p;
		public DownAction(Player player){
			this.p = player;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("s pressed");
			p.setPlayerY(p.getPlayerY() + 10);
			p.refreshPlayer();
			//Communication player moved
		}
	}
	
	private class LeftAction extends AbstractAction {
		private Player p;
		public LeftAction(Player player){
			this.p = player;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("a pressed");
			p.setPlayerX(p.getPlayerX() - 10);
			p.refreshPlayer();
			//Communication player moved
		}
	}
	
	private class RightAction extends AbstractAction {
		private Player p;
		public RightAction(Player player){
			this.p = player;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("d pressed");
			p.setPlayerX(p.getPlayerX() + 10);
			p.refreshPlayer();
			//Communication player moved
		}
	}
	
	private class SpaceAction extends AbstractAction {
		private Player p;
		public SpaceAction(Player player){
			this.p = player;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("space pressed");
			// Communication Bomb planted
		}
	}

}

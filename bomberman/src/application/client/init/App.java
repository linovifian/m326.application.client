package application.client.init;

import application.client.controllers.BombermanClient;

public class App {

	public static final int DIMENSION = 800;
	
	// Initial Start
	public static void main(String[] args) {
		BombermanClient bombermanClient = BombermanClient.getInstance();
		bombermanClient.startFlow();
	}
	
}
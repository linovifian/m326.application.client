package application.client.views;


import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import application.client.controllers.BombermanClient;

public class Air extends Tile{

	public Air(int x, int y) {
		super(x, y);
	}

	@Override
	public JLabel drawTile() {
		int size = BombermanClient.SIZE;
		image = new JLabel();
		image.setIcon(new ImageIcon(new ImageIcon("src/application/client/views/air.png").getImage().getScaledInstance(size, size, Image.SCALE_FAST)));
		image.setBounds(x * size, y * size, size, size);
		return image;		
	}
	
}

package application.client.views;

import javax.swing.JLabel;
import javax.swing.JPanel;

import application.client.controllers.BombermanClient;

public abstract class Tile extends JPanel{
	
	// Instance variables
	protected int x;
	protected int y;
	protected boolean passable;
	protected JLabel image;
	
	public Tile(int x, int y){
		this.setX(x);
		this.setY(y);
	}
	
	// Getters
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public boolean isPassable() {
		return passable;
	}
	
	// Setters
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}
	public void setPassable(boolean passable) {
		this.passable = passable;
	}	
	
	public void refreshTile(){
		int size = BombermanClient.SIZE;
		image.setBounds(this.x , this.y, size, size);
		image.repaint();
	}
	
	public abstract JLabel drawTile();
		
}

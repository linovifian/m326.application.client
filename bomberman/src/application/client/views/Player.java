package application.client.views;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import application.client.controllers.BombermanClient;

public class Player extends JLabel {
	
	private String name; 
	private int number;
	private int x;
	private int y;
	private Boolean alive;
	private JLabel image;
	private Boolean bombSet;
	
	// Section Getter and Setters
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getPlayerX() {
		return x;
	}

	public void setPlayerX(int x) {
		this.x = x;
	}

	public int getPlayerY() {
		return y;
	}

	public void setPlayerY(int y) {
		this.y = y;
	}

	public Boolean isAlive() {
		return alive;
	}

	public void setAlive(Boolean alive) {
		this.alive = alive;
	}
	
	public Boolean isBombSet() {
		return bombSet;
	}

	public void setBombSet(Boolean bombSet) {
		this.bombSet = bombSet;
	}
	
	// End Section Getter and Setters

	public Player(String name, int number){
		this.alive = true;
		this.number = number;
		this.setIcon(new ImageIcon(new ImageIcon("src/application/client/views/player" + this.number + ".png").getImage().getScaledInstance(BombermanClient.SIZE, BombermanClient.SIZE, Image.SCALE_FAST)));
	}
	
	public JLabel drawPlayer(){
		int size = BombermanClient.SIZE;
		image = new JLabel();
		image.setIcon(new ImageIcon(new ImageIcon("src/application/client/views/player" + this.number + ".png").getImage().getScaledInstance(size, size, Image.SCALE_FAST)));	
		image.setBounds(this.x , this.y, size, size);
		image.setMaximumSize(new Dimension(size, size));
		return image;
	}
	
	public void refreshPlayer(){
		int size = BombermanClient.SIZE;
		image.setBounds(this.x , this.y, size, size);
		image.repaint();
	}

}

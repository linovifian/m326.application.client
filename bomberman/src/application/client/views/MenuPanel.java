package application.client.views;

import java.awt.Font;
import javax.swing.*;

import application.client.controllers.BombermanClient;
import application.client.init.App;

import java.awt.Color;

public class MenuPanel extends JPanel{
	
	// Instance variables
	private JLabel lblTitle = new JLabel();
	private JButton btnFindGame = new JButton();
	private JButton btnExit = new JButton();
	private JLabel lblName = new JLabel();
	private JLabel lblError = new JLabel();
	private JTextField txtPlayerName = new JTextField();
	
	// Constructor
	public MenuPanel(){
		this.setBounds(0, 0, App.DIMENSION, App.DIMENSION);
		this.initialiseComponents();
		this.setLayout(null);
	}
	
	private void initialiseComponents(){
		// Prepare and add title label
		this.lblTitle.setText(BombermanClient.TITLE);
		this.lblTitle.setFont(new Font("Arial", Font.BOLD, 28));
		this.lblTitle.setForeground(Color.darkGray);
		this.lblTitle.setSize(this.getWidth() / 3, this.getHeight() / 10);
		this.lblTitle.setLocation((this.getWidth() - this.lblTitle.getWidth()) / 2, this.getHeight() / 8);
		this.lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(this.lblTitle);
		
		this.lblName.setText("Name");
		this.lblName.setFont(new Font("Arial", Font.BOLD, 14));
		this.lblName.setForeground(Color.darkGray);
		this.lblName.setBounds(this.getWidth() / 3, this.getHeight() / 3, this.getWidth() / 12, this.getHeight() / 20);
		this.lblName.setHorizontalAlignment(SwingConstants.LEFT);
		this.add(this.lblName);
		
		this.txtPlayerName.setText("Playername");
		this.txtPlayerName.setFont(new Font("Arial", Font.PLAIN, 14));
		this.txtPlayerName.setForeground(Color.darkGray);
		this.txtPlayerName.setBounds(this.getWidth() / 2 - this.getWidth() / 12, this.getHeight() / 3, this.getWidth() / 4, this.getHeight() / 20);
		this.add(txtPlayerName);
		
		this.lblError.setText("Couldn't find game");
		this.lblError.setFont(new Font("Arial", Font.BOLD, 14));
		this.lblError.setForeground(Color.red);
		this.lblError.setBounds(this.getWidth()/3, (int) (this.getHeight() * 0.60), this.getWidth() / 3, this.getHeight() / 10);
		this.lblError.setVisible(false);
		this.add(lblError);
		
		// Prepare and add find game button
		this.btnFindGame.setText("Find game");
		this.btnFindGame.setFont(new Font("Arial", Font.BOLD, 16));
		this.btnFindGame.setForeground(Color.darkGray);
		this.btnFindGame.setBackground(Color.LIGHT_GRAY);
		this.btnFindGame.setBounds(this.getWidth() / 3, this.getHeight() / 2, this.getWidth() / 3, this.getHeight() / 10);
		this.add(btnFindGame);
		
		// Prepare and add find exit button
		this.btnExit.setText("Close game");
		this.btnExit.setFont(new Font("Arial", Font.BOLD, 16));
		this.btnExit.setForeground(Color.darkGray);
		this.btnExit.setBackground(Color.LIGHT_GRAY);
		this.btnExit.setBounds(this.getWidth() / 3, (int) (this.getHeight() * 0.75), this.getWidth() / 3, this.getHeight() / 10);
		this.add(btnExit);
		
		this.repaint();
	}
	
	public JButton getBtnExit(){
		return this.btnExit;
	}
	
	public JButton getBtnFindGame(){
		return this.btnFindGame;
	}
	
	public String getPlayerName(){
		return this.txtPlayerName.getText();
	}
	
	public void setErrorLabelVisible(){
		this.lblError.setVisible(true);
	}
}

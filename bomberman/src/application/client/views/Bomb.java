package application.client.views;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import application.client.controllers.BombermanClient;

public class Bomb extends Tile{
	
	public Bomb(int x, int y) {
		super(x, y);
	}

	// Instance variables
	private int timer;

	// Getters
	public int getTimer() {
		return timer;
	}

	// Setters
	public void setTimer(int timer) {
		this.timer = timer;
	}

	@Override
	public JLabel drawTile() {
		int size = BombermanClient.SIZE;
		image = new JLabel();
		image.setIcon(new ImageIcon(new ImageIcon("src/application/client/views/bomb.png").getImage().getScaledInstance(size, size, Image.SCALE_FAST)));
		image.setBounds(x * size, y * size, size, size);
		return image;
	}
	
	
}

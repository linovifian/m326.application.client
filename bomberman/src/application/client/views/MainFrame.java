package application.client.views;

import javax.swing.*;

import application.client.controllers.BombermanClient;

public class MainFrame extends JFrame{
	
	public static MessagePanel messagePanel;
	
	// Constructor
	public MainFrame(int x, int y, int width, int height){
		// Configurations
		this.setBounds(x, y, width, height);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(BombermanClient.TITLE);
		this.setResizable(false);
		this.setLayout(null);
		this.setVisible(true);
		this.messagePanel = new MessagePanel(height, width);
		this.add(this.messagePanel);
	}	
}

package application.client.views;


import java.awt.Dimension;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import application.client.controllers.BombermanClient;

public class Block extends Tile {

	public Block(int x, int y, boolean destroyable) {
		super(x, y);
		this.setDestroyable(destroyable);
	}

	// Instance variables
	private boolean destroyable;

	// Getters
	public boolean isDestroyable() {
		return destroyable;
	}
	
	// Setters
	public void setDestroyable(boolean destroyable) {
		this.destroyable = destroyable;
	}

	@Override
	public JLabel drawTile() {
		int size = BombermanClient.SIZE;
		if (this.isDestroyable()){
			image = new JLabel();
			image.setIcon(new ImageIcon(new ImageIcon("src/application/client/views/rock.png").getImage().getScaledInstance(size, size, Image.SCALE_FAST)));
		} else{
			image = new JLabel();
			image.setIcon(new ImageIcon(new ImageIcon("src/application/client/views/block.png").getImage().getScaledInstance(size, size, Image.SCALE_FAST)));
		}		
		image.setBounds(x * size, y * size, size, size);
		image.setMaximumSize(new Dimension(size, size));
		return image;		
	}
	
}

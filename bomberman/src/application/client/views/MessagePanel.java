package application.client.views;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class MessagePanel extends JPanel{
	
	private JLabel lblTopMessage = new JLabel();
	private JLabel lblBottomMessage = new JLabel();
	
	public MessagePanel(int y, int width){
		this.setBounds(0, y, width, 75);
		
		this.lblTopMessage.setBounds(0, 0, this.getWidth(), 25);
		this.lblTopMessage.setBackground(Color.lightGray);
		this.lblTopMessage.setVerticalTextPosition(SwingConstants.CENTER);
		this.lblTopMessage.setHorizontalTextPosition(SwingConstants.CENTER);
		this.lblTopMessage.setFont(new Font("Arial", Font.BOLD, 20));
		
		this.lblBottomMessage.setBounds(0, 25, this.getWidth(), 25);
		this.lblBottomMessage.setBackground(Color.lightGray);
		this.lblBottomMessage.setVerticalTextPosition(SwingConstants.CENTER);
		this.lblBottomMessage.setHorizontalTextPosition(SwingConstants.CENTER);
		this.lblBottomMessage.setFont(new Font("Arial", Font.BOLD, 20));
		this.lblBottomMessage.setForeground(Color.darkGray);
	}
	
	public void displayMessage(String text){
		this.lblBottomMessage.setText(lblTopMessage.getText());
		this.lblTopMessage.setText(text);
	}

}

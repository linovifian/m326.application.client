package application.client.models;

/**
 * This enum is used to specify the direction the player is headed
 * @author vmadmin
 *
 */
public enum Direction {
	UP("up"),
	DOWN("down"),
	LEFT("left"),
	RIGHT("right"),
	;
	
	private final String direction;
	
	Direction(String direction){
		this.direction = direction;
	}
	
}

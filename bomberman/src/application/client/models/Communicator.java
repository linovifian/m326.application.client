package application.client.models;


import network.Message;
import network.ServerProxy;
import network.client.ClientApplicationInterface;
import protocol.clientToServer.*;

/**
 * low level Communication Class. Handles messages sent from and to server
 * class and its functions are only visible for Package, as the user should use the Adapters for this class 
 * @author vmadmin
 *
 */
class Communicator implements ClientApplicationInterface{

	private static Communicator instance;
	private ServerProxy proxy;
	private Dispatcher dispatcher;
	
	// private constructor, to force the use of getInstance 
	private Communicator(){
		proxy = new ServerProxy(this,"127.0.0.1");
		dispatcher = new Dispatcher();
	}
	
	// returns an instance of COmmunicator, when instance is null then create a new one
	static Communicator getInstance(){
		if(instance == null){
			instance = new Communicator();
		}
		return instance;
	}
	
	// delegates the handling of messages to Dispatcher
	@Override
	public void handleMessage(Message message) {
		dispatcher.dispatch(message);
	}
	
	// sends joinGame message to server
	public void joinGame(String playerName){
		JoinGame joinGame = new JoinGame(playerName);
		this.proxy.send(joinGame);
	}
	
	// sends move message to server
	public void move(String playerName, Direction direction){
		MovePlayer movePlayer = new MovePlayer(playerName, direction.name());		
		proxy.send(movePlayer);
	}
	
	// sends dropbomb message to server
	public void dropBomb(String playerName, int x, int y){
		DropBomb dropBomb = new DropBomb(playerName, x, y);		
		proxy.send(dropBomb);
	}
	
}

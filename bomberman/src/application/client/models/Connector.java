package application.client.models;

/**
 * This interface is a layer on top of the Communicator.
 * Its methods are used to connect or disconnect from the Server 
 * @author vmadmin
 *
 */
public interface Connector {

	public static void joinGame(String playerName){
		Communicator.getInstance().joinGame(playerName);
	}
	
	//TODO implement remaining methods
}

package application.client.models;

import application.client.controllers.MenuController;
import application.client.views.Air;
import application.client.views.Block;
import application.client.views.Tile;
import network.Message;
import protocol.serverToClient.*;
/**  
 * @author vmadmin
 *
 */
class Dispatcher {

	// determine which type of message given message is
	public void dispatch(Message message) {
		if(message instanceof PlayerJoined){
			playerHasJoined((PlayerJoined) message);
		} else if(message instanceof PlayerMoved){
			playerWasMoved((PlayerMoved) message);
		} else if(message instanceof StartGame){
			gameWasStarted((StartGame) message);
		} else if(message instanceof PlayerHit){
			playerWasHit((PlayerHit) message);
		} else if(message instanceof GameOver){
			gameIsOver((GameOver) message);
		}
	}
	
	// converts labyrinth into 2-Dimensional TileArray
	private Tile[][] getTiles(StartGame startGame){
		
		int[][] labyrinth = startGame.getLabyrinth();
		int length1D = labyrinth.length;
		int length2D = labyrinth[0].length;
		Tile[][] tiles = new Tile[length1D][length2D];
		
		// iterate over labyrinth
		for(int i = 0; i < length1D; i++){
			for(int j = 0; j < length2D; j++){
				// check what BlockType tile is
				if(labyrinth[i][j] == BlockType.AIR.ordinal()){
					// convert to Air Tile
					tiles[i][j] = new Air(i, j);
				} else if(labyrinth[i][j] == BlockType.EARTH.ordinal()){
					// convert to Earth Tile
					tiles[i][j] = new Block(i, j, true);
				} else if(labyrinth[i][j] == BlockType.ROCK.ordinal()){
					// convert to Rock Tile
					tiles[i][j] = new Block(i, j, false);
				} else {
					// throw when invalid argument was provided
					throw new UnsupportedOperationException("Unsupported type: "+labyrinth[i][j]);
				}
			}
		}
		
		// return TileArray
		return tiles;
	}
	
	private void playerHasJoined(PlayerJoined playerJoined){
		// TODO implement
		throw new UnsupportedOperationException("This method is not implemented");
	}
	
	private void playerWasMoved(PlayerMoved playerMoved){
		// TODO implement
		throw new UnsupportedOperationException("This method is not implemented");
	}
	
	private void gameWasStarted(StartGame startGame){
		// get labyrinth
		Tile[][] tiles = getTiles(startGame);
		
		// call menuController with tiles as argument
		MenuController.getInstance().displayGamePanel(tiles);
	}
	
	private void playerWasHit(PlayerHit playerHit){
		// TODO implement
		throw new UnsupportedOperationException("This method is not implemented");
	}
	
	private void gameIsOver(GameOver gameOver){
		// TODO implement
		throw new UnsupportedOperationException("This method is not implemented");
	}
}

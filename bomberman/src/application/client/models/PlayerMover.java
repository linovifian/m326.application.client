package application.client.models;

/**
 * This interface is a layer on top of the Communicator.
 * Each method moves the player to a different direction, 
 * by calling the move event on the communicator with the selected Direction
 * @author vmadmin
 *
 */
public interface PlayerMover {
	
	public static void moveUp(String playerName){
		Communicator.getInstance().move(playerName, Direction.UP);
	}
	
	public static void moveDown(String playerName){
		Communicator.getInstance().move(playerName, Direction.DOWN);	
	}
		
	public static void moveLeft(String playerName){
		Communicator.getInstance().move(playerName, Direction.LEFT);
	}
	
	public static void moveRight(String playerName){
		Communicator.getInstance().move(playerName, Direction.RIGHT);
	}
	
}

package application.client.models;

/**
 * this enum is used to map the int values from the server, for easier access and usability 
 * @author vmadmin
 *
 */
public enum BlockType {
	AIR,
	EARTH,
	ROCK,
	;
}
